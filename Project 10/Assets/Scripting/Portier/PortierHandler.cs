﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortierHandler : MonoBehaviour
{
    [SerializeField] private UnitManager unitManager;

    public void AllowIn()
    {
        
        var test = unitManager.GetFrontBaseUnit();
        if (test == null)
            return;
        Debug.Log(test.MyFlaw);       
        unitManager.RemoveFrontInQue(true);
    }

    public void DenyIn()
    {
        var test = unitManager.GetFrontBaseUnit();
        if (test == null)
            return;
        Debug.Log(test.MyFlaw);
        unitManager.RemoveFrontInQue(false);
    }
}
