﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DataController : MonoBehaviour
{
    [SerializeField] private RaceFile racefile;
    [SerializeField] private MessagesFile messageFile;

    public static Dictionary<Race, RaceData> RaceInformation;
    public static Dictionary<Flaw, MessageData> MessageInformation;

    private const string RaceFileName = "AllroundData.json";
    private const string MessageFileName = "AllroundMessageData.json";

    private void Awake()
    {
        LoadJsonFile();
    }

    private void LoadJsonFile()
    {
        string racefilePath = Path.Combine(Application.streamingAssetsPath, RaceFileName);
        string messagefilePath = Path.Combine(Application.streamingAssetsPath, MessageFileName);

        if (File.Exists(racefilePath))
        {
            string dataAsJson = File.ReadAllText(racefilePath);

            RaceFile loadedData = JsonUtility.FromJson<RaceFile>(dataAsJson);

            racefile = loadedData;
            InitiliazeRaceInformation();
        }
        else
        {
            Debug.LogError("Cannot load game data!");
        }

        if (File.Exists(messagefilePath))
        {
            string dataAsJson = File.ReadAllText(messagefilePath);

            MessagesFile loadedData = JsonUtility.FromJson<MessagesFile>(dataAsJson);
            messageFile = loadedData;
            InitiliazeMessageInformation();
        }
        else
        {
            Debug.LogError("Cannot load funny messages!");
        }
    }

    private void InitiliazeRaceInformation()
    {
        RaceInformation = new Dictionary<Race, RaceData>();
        for (int i = 0; i < racefile.AllRaceData.Length; i++)
        {
            RaceInformation.Add((Race)Enum.Parse(typeof(Race), racefile.AllRaceData[i].RaceDef), racefile.AllRaceData[i]);
        }
    }

    private void InitiliazeMessageInformation()
    {
        MessageInformation = new Dictionary<Flaw, MessageData>();
        for (int i = 0; i < messageFile.AllMessageData.Length; i++)
        {
            MessageInformation.Add((Flaw)Enum.Parse(typeof(Flaw), messageFile.AllMessageData[i].FlawDef), messageFile.AllMessageData[i]);
        }
    }
}

public enum Race
{
    Fairy,
    Elf,
    Human,
    Dwarf
}

[System.Serializable]
public class MessagesFile
{
    public MessageData[] AllMessageData;
}

[System.Serializable]
public class MessageData
{
    public string FlawDef;
    public string[] CaughtStrings;
}


[System.Serializable]
public class RaceFile
{
    public RaceData[] AllRaceData;
}
 
[System.Serializable]
public class RaceData
{
    public string RaceDef;
    public string[] Names;
    public string[] BirthPlaces;
    public int AvgLength;
    public string[] DeniedMessages;
    public string[] RandomMessages;
}

