﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelInformation", menuName = "GenerationLevel", order = 1)]
public class GenerationLevel : ScriptableObject
{
    [Range(1,100)]
    [SerializeField] public int validPrecentage = 75;
    public int ValidPrecentage { get { return validPrecentage; } }
    [SerializeField] public int totalCharacters = 25;
    public int TtotalCharacters { get { return totalCharacters; } }
}
