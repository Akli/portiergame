﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Flaw
{
    Toxicated,
    UnderAged,
    FakeID,
    WrongRace
}

public class Toxcicated : IFlaw
{
    public string CaughtMessage()
    {
        return MessageUtility.GetCaughtMessage(FlawType());
    }

    public Flaw FlawType()
    {
        return Flaw.Toxicated;
    }
}

public class Underaged : IFlaw
{
    public string CaughtMessage()
    {
        return MessageUtility.GetCaughtMessage(FlawType());
    }

    public Flaw FlawType()
    {
        return Flaw.UnderAged;
    }
}

public class FakeID : IFlaw
{
    public string CaughtMessage()
    {
        return MessageUtility.GetCaughtMessage(FlawType());
    }

    public Flaw FlawType()
    {
        return Flaw.FakeID;
    }
}

public class WrongRace : IFlaw
{
    public string CaughtMessage()
    {
        return MessageUtility.GetCaughtMessage(FlawType());
    }

    public Flaw FlawType()
    {
        return Flaw.WrongRace;
    }
}
