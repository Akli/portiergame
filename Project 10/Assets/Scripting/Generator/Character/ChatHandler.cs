﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class ChatHandler : MonoBehaviour
{
    [SerializeField] private TextMeshPro textField;
    [SerializeField] private Animator chatAnimator;
    [HideInInspector] public bool IsActive;

    public void DisplayChat(UnitInformation information)
    {
        IsActive = true;
        StartCoroutine(ChatLoop(information));
    }

    IEnumerator ChatLoop(UnitInformation information)
    {
        while (true)
        {
            int rnd = Random.Range(5, 15);
            yield return new WaitForSeconds(rnd);
            textField.text = MessageUtility.GetRandomMessage(information.Race);
            chatAnimator.SetTrigger("DisplayText");
        }
    }

    public void ForceDisplayMessage(ChatType type, BaseUnit unit)
    {
        IsActive = false;
        StopAllCoroutines();

        switch (type)
        {
            case ChatType.Caught:
                textField.text = MessageUtility.GetCaughtMessage(unit.MyFlaw.FlawType());
                break;
            case ChatType.FalslyDenied:
                textField.text = MessageUtility.GetFalselyDeniedMessage(unit.information.Race);
                break;
        }

        chatAnimator.SetTrigger("DisplayText");
    }
}

public enum ChatType
{
    Caught,
    FalslyDenied
}


