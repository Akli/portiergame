﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[System.Serializable]
public class UnitInformation
{
    public Race Race;
    public string Name;
    public string BirthPlace;
    public string IdCode;
    public int Age;
    public int Length;
    public bool IsMale;
    public Sprite Verification;

    public void FillValidInformation()
    {
        Race = GenerationUtility.GetRandomRace(true);
        Name = GenerationUtility.GetRandomName(Race);
        BirthPlace = GenerationUtility.GetRandomBirthPlace(Race);
        Age = GenerationUtility.GetRandomAge(true, 18);
        IsMale = GenerationUtility.IsMale();
        Length = GenerationUtility.GetRandomLength(Race);
        IdCode = GenerationUtility.GetRandomCode();
        Verification = GenerationUtility.VerificationImage(true);
    }

    public void FillInValidInformation(IFlaw myFlaw)
    {
        Race = myFlaw.FlawType() == Flaw.WrongRace ? GenerationUtility.GetRandomRace(false) : GenerationUtility.GetRandomRace(true, LevelGenerator.instance.DeniedRaces[0]);

        Name = GenerationUtility.GetRandomName(Race);
        BirthPlace = GenerationUtility.GetRandomBirthPlace(Race);
        IsMale = GenerationUtility.IsMale();
        Length = GenerationUtility.GetRandomLength(Race);
        IdCode = GenerationUtility.GetRandomCode();

        Age = GenerationUtility.GetRandomAge(myFlaw.FlawType() != Flaw.UnderAged, 18);
        Verification = GenerationUtility.VerificationImage(myFlaw.FlawType() != Flaw.FakeID);
    }
}
