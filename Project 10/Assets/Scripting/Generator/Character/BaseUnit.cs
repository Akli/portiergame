﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(ChatHandler))]
public class BaseUnit : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    private bool isActive;
    private ChatHandler chatHandler;
    private UnitBehaviour unitBehaviour;

    public IFlaw MyFlaw;
    [HideInInspector] public UnitInformation information;

    private void Awake()
    {
        chatHandler = GetComponent<ChatHandler>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        unitBehaviour = transform.parent.GetComponent<UnitBehaviour>();
    }

    private void Start()
    {
    }

    public void InitliazeUnit(bool isValid)
    {
        if (isValid)
        {
            MyFlaw = null;
            information.FillValidInformation();

        }
        else
        {
            int random = Random.Range(0, Enum.GetNames(typeof(Flaw)).Length);
            MyFlaw = GameManager.Flaws[(Flaw)random];
            information.FillInValidInformation(MyFlaw);
        }

        isActive = true;
    }

    #region MouseStuff

    void OnMouseDown()
    {
        if (information != null && unitBehaviour.CurrentNode == 0)
            UnitManager.OnUnitClick(this);
            //Identinity.Instance.DisplayIdentinity(information);
    }

    void OnMouseOver()
    {
        if (unitBehaviour.CurrentNode == 0)
            spriteRenderer.color = Color.cyan;
    }

    void OnMouseExit()
    {
        if (unitBehaviour.CurrentNode == 0)
            spriteRenderer.color = Color.white;
    }

    public void StartChat()
    {
        chatHandler = GetComponent<ChatHandler>();
        chatHandler.DisplayChat(information);
    }

    #endregion

    void OnGUI()
    {
        if (MyFlaw != null)
            GUI.Label(new Rect(20, 20, 200, 50), "FLAW: " + MyFlaw.FlawType());
    }
}