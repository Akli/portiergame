﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnitState
{
    void OnEnter();
    void Update();
    void OnExit();
}

public class UnitBehaviour : MonoBehaviour
{
    IUnitState m_State;

    [SerializeField] GameObject Human;
    [SerializeField] GameObject Human2;
    [SerializeField] GameObject Dwarf;
    [SerializeField] GameObject Fairy;
    [SerializeField] GameObject Elf;

    [SerializeField] Animator m_Animator;

    [SerializeField] GameObject m_SpriteObject;
    public GameObject SpriteObject { get { return m_SpriteObject; } }

    [SerializeField] float m_WalkSpeed;
    public float WalkSpeed { get { return m_WalkSpeed; } }

    private BaseUnit m_BaseUnit;
    public BaseUnit BaseUnit { get { return m_BaseUnit; } }

    int m_CurrentNode;
    public int CurrentNode { get { return m_CurrentNode; } set { m_CurrentNode = value; } }

    UnitManager m_UnitManager;
    public UnitManager UnitManager { get { return m_UnitManager; } set { m_UnitManager = value; } }

    private void Awake()
    {
       
        
    }

    void Start()
    {

    }

    void Update()
    {
        if(m_State!= null)
        m_State.Update();
    }

    public void SwitchState(IUnitState state)
    {
        if (m_State != null)
            m_State.OnExit();
        m_State = state;
        m_State.OnEnter();
    }

    public void SetAnimation(string animation)
    {
        m_Animator.Play(animation, 0);
    }

    public void SetSprite(Race race)
    {
        GameObject character;
        int variation = Random.Range(0, 2);
        switch (race)
        {
            case Race.Fairy:
                character = Instantiate(Fairy, transform);
                character.transform.SetParent(transform);
                m_Animator = character.GetComponent<Animator>();
                m_SpriteObject = character;
                break;
            case Race.Dwarf:
                character = Instantiate(Dwarf, transform);
                character.transform.SetParent(transform);
                m_Animator = character.GetComponent<Animator>();
                m_SpriteObject = character;
                break;
            case Race.Elf:
               
                character = Instantiate(Elf, transform);
                character.transform.SetParent(transform);
                m_Animator = character.GetComponent<Animator>();
                m_SpriteObject = character;
                break;
            case Race.Human:
                if (variation == 0)
                    character = Instantiate(Human, transform);
                else
                    character = Instantiate(Human2, transform);
                character.transform.SetParent(transform);
                m_Animator = character.GetComponent<Animator>();
                m_SpriteObject = character;
                break;
        }
      
        m_State = new UnitIdle(this);
        m_BaseUnit = GetComponentInChildren<BaseUnit>();
    }

    public void AllowIn(bool isAllowed)
    {
        switch (isAllowed)
        {
            case true:
                SwitchState(new Accept(this));
                break;
            case false:
                SwitchState(new Denied(this));
                break;
           
        }
    }
}

public class UnitIdle : IUnitState
{
    UnitBehaviour m_RefObject;

    public UnitIdle(UnitBehaviour refObject)
    {
        m_RefObject = refObject;
    }
    public void OnEnter()
    {
        //if(m_RefObject.GetComponent<>)
        m_RefObject.SetAnimation("idle");
    }
    public void Update()
    {
        if (m_RefObject.UnitManager.UnitsQue[m_RefObject.CurrentNode - 1] == null)
        {
            m_RefObject.SwitchState(new UnitWalk(m_RefObject, m_RefObject.UnitManager.Spline.QuePosition[m_RefObject.CurrentNode - 1]));
        }
    }
    public void OnExit()
    {

    }
}

public class UnitWalk : IUnitState
{
    UnitBehaviour m_RefObject;
    Vector3 m_Destination;

    public UnitWalk(UnitBehaviour refObject, Vector3 destination)
    {
        m_RefObject = refObject;
        m_Destination = destination;
    }
    public void OnEnter()
    {
        if (m_RefObject.CurrentNode > 0)
        {
            if (Mathf.Sign(Vector3.Dot(m_RefObject.UnitManager.Spline.QueDirections[m_RefObject.CurrentNode - 1], Vector3.forward)) <= 0)
            {
                m_RefObject.SpriteObject.transform.localEulerAngles = new Vector3(0, 180, 0);
            }
            else
            {
                m_RefObject.SpriteObject.transform.localEulerAngles = new Vector3(0, 0, 0);
            }
        }

        m_RefObject.SetAnimation("walk");
    }
    public void Update()
    {
        if (m_RefObject.CurrentNode == 0)
        {
            m_RefObject.SwitchState(new UnitFrontInLine(m_RefObject));
        }
        else
        {
            m_RefObject.transform.position = Vector3.MoveTowards(m_RefObject.transform.position, m_Destination, m_RefObject.WalkSpeed * Time.deltaTime);

            if (m_RefObject.transform.position == m_Destination)
            {
                m_RefObject.UnitManager.MoveUnitPositionInQue(m_RefObject, m_RefObject.CurrentNode - 1);

                if (m_RefObject.CurrentNode != 0)
                    m_RefObject.SwitchState(new UnitWalk(m_RefObject, m_RefObject.UnitManager.Spline.QuePosition[m_RefObject.CurrentNode - 1]));
            }

            if (m_RefObject.CurrentNode != 0)
                if (m_RefObject.UnitManager.UnitsQue[m_RefObject.CurrentNode - 1] != null)
                {
                    m_RefObject.SwitchState(new UnitIdle(m_RefObject));
                }

        }
    }
    public void OnExit()
    {

    }
}

public class UnitFrontInLine : IUnitState
{
    UnitBehaviour m_RefObject;

    public UnitFrontInLine(UnitBehaviour refObject)
    {
        m_RefObject = refObject;
    }
    public void OnEnter()
    {
        m_RefObject.SetAnimation("idle");
    }
    public void Update()
    {

    }
    public void OnExit()
    {

    }
}

public class Accept : IUnitState
{
    UnitBehaviour m_RefObject;

    public Accept(UnitBehaviour refObject)
    {
        m_RefObject = refObject;
    }
    public void OnEnter()
    {
        m_RefObject.SetAnimation("walk");
    }
    public void Update()
    {
        m_RefObject.transform.Translate(-Vector3.right * Time.deltaTime * 50);
    }
    public void OnExit()
    {

    }
}

public class Denied : IUnitState
{
    UnitBehaviour m_RefObject;

    public Denied(UnitBehaviour refObject)
    {
        m_RefObject = refObject;
    }
    public void OnEnter()
    {
        m_RefObject.SetAnimation("walk");
        m_RefObject.SpriteObject.transform.localEulerAngles = new Vector3(0, 180, 0);
    }
    public void Update()
    {
        m_RefObject.transform.Translate(Vector3.forward * Time.deltaTime * 50);
    }
    public void OnExit()
    {

    }
}
