﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFlaw
{
    string CaughtMessage();
    Flaw FlawType();
    
}
