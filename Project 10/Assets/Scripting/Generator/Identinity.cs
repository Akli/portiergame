﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Animator))]
public class Identinity : MonoBehaviour
{
    [Header("UI Connection")]
    [SerializeField] Image id_Icon;
    [SerializeField] Image id_Verfication;
    [SerializeField] Image id_BackGround;
    [SerializeField] TextMeshProUGUI id_Name;
    [SerializeField] TextMeshProUGUI id_Race;
    [SerializeField] TextMeshProUGUI id_Length;
    [SerializeField] TextMeshProUGUI id_Age;
    [SerializeField] TextMeshProUGUI id_BirthPlace;
    [SerializeField] TextMeshProUGUI id_CodeNumber;

    public static Identinity Instance;

    private Animator anim;
    private bool isActive;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        Instance = this;
    }

    public void DisplayIdentinity(UnitInformation information)
    {
        if (isActive)
        {
            HideIdentinity();
            return;
        }

        anim.SetTrigger("FadeIn");
        id_Name.text = "Name: " + information.Name;
        id_Race.text = "Race: " + information.Race;
        id_Length.text = "Length: " + information.Length + " cm";
        id_Age.text = "Age: " + information.Age;
        id_BirthPlace.text = "BirthPlace: " + information.BirthPlace;
        id_CodeNumber.text = "Code: " + information.IdCode;
        id_Verfication.sprite = information.Verification;
        isActive = true;

    }

    public void HideIdentinity()
    {
        if (!isActive) return;

        anim.SetTrigger("FadeOut");
        isActive = false;
    }
}
