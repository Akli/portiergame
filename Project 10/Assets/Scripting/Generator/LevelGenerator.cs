﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct AgeRange
{
    public AgeRange(int minimum, int maximum)
    {
        Min = minimum;
        Max = maximum;
    }
    public int Min;
    public int Max;
}

public class LevelGenerator : MonoBehaviour
{
    public static LevelGenerator instance;
    [Header("Criteria")]
    private Race[] m_DeniedRaces;
    public Race[] DeniedRaces { get { return m_DeniedRaces; } }
    private AgeRange m_AgeRange;
    public AgeRange AgeRange { get { return m_AgeRange; } }

    [Header("Settings")]
    [SerializeField] private int m_AmountOfPossibleDeniedRaces;
    [SerializeField] private AgeRange m_MinimumAgeRange;
    [SerializeField] private AgeRange m_MaximumAgeRange;

    private void Awake()
    {
        CreateInstance();
        Init();
    }

    void Init()
    {
       
        m_AgeRange = new AgeRange(Random.Range(m_MinimumAgeRange.Min, m_MinimumAgeRange.Max + 1), Random.Range(m_MaximumAgeRange.Min, m_MaximumAgeRange.Max + 1));
        GenerateDeniedRaces();
    }

    void GenerateDeniedRaces()
    {
        if (m_AmountOfPossibleDeniedRaces >= Race.GetNames(typeof(Race)).Length)
            m_AmountOfPossibleDeniedRaces = Race.GetNames(typeof(Race)).Length -1;
        if(m_AmountOfPossibleDeniedRaces <1)
            m_AmountOfPossibleDeniedRaces = 1;

        m_DeniedRaces = new Race[Random.Range(1, m_AmountOfPossibleDeniedRaces + 1)];
        Race race = 0;
        for (int i = 0; i < m_DeniedRaces.Length; i++)
        {
            do
            {
                race = (Race)Random.Range(0, Race.GetNames(typeof(Race)).Length);
            }
            while (UnityEditor.ArrayUtility.Contains(m_DeniedRaces, race));
            m_DeniedRaces[i] = race;
        }
    }

    void CreateInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
}
