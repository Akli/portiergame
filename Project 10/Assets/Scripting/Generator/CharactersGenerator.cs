﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactersGenerator : MonoBehaviour
{
    [SerializeField] BaseUnit m_UnitPrefab;
    [SerializeField] GenerationLevel m_Rate;
    [SerializeField] UnitManager m_UnitManager;
   


    private void Start()
    {
        float validAmout = m_Rate.totalCharacters * 0.01f * m_Rate.validPrecentage;
        float invalidAmout = m_Rate.totalCharacters * 0.01f * (100 - m_Rate.validPrecentage);

        for (int i = 0; i < validAmout; i++)
        {
            UnitBehaviour unit = Instantiate(m_UnitPrefab.transform.parent.GetComponent<UnitBehaviour>());
            BaseUnit baseUnit = unit.GetComponentInChildren<BaseUnit>();
            baseUnit.InitliazeUnit(true);
            unit.SetSprite(baseUnit.information.Race);
            m_UnitManager.AddToBuffer(unit);
        }

        for (int i = 0; i < invalidAmout; i++)
        {
            UnitBehaviour unit = Instantiate(m_UnitPrefab.transform.parent.GetComponent<UnitBehaviour>());
            BaseUnit baseUnit = unit.GetComponentInChildren<BaseUnit>();
            baseUnit.InitliazeUnit(false);
            unit.SetSprite(baseUnit.information.Race);
            m_UnitManager.AddToBuffer(unit);
        }

        ListUtility.Shuffle(m_UnitManager.UnitsBuffer);
    }
}

public static class ListUtility
{
    private static System.Random rng = new System.Random();

    public static void Shuffle<T>(IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
