﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public static class GenerationUtility
{
    public static string GetRandomName(Race race)
    {
        return !DataController.RaceInformation.ContainsKey(race) ? "This race does not exist!" :
            DataController.RaceInformation[race].Names[RandomNumber(DataController.RaceInformation[race].Names.Length)];
    }

    public static string GetRandomBirthPlace(Race race)
    {
        return !DataController.RaceInformation.ContainsKey(race) ? "This race does not exist!" :
            DataController.RaceInformation[race].BirthPlaces[RandomNumber(DataController.RaceInformation[race].BirthPlaces.Length)];
    }

    public static Race GetRandomRace(bool isValid, Race deniedRace = Race.Dwarf)
    {
        if(isValid)
            return (Race)Random.Range(0, Enum.GetNames(typeof(Race)).Length);
        else
        {
            return deniedRace;
        }
    }

    public static int GetRandomLength(Race race)
    {
        int fluctuation = (int)(DataController.RaceInformation[race].AvgLength * 0.08f);

        return !DataController.RaceInformation.ContainsKey(race) ? 0 : Random.Range(DataController.RaceInformation[race].AvgLength - fluctuation,
            DataController.RaceInformation[race].AvgLength + fluctuation);
    }

    public static bool IsMale()
    {
        return Random.Range(0,2) == 1;
    }

    public static Sprite VerificationImage(bool isValid)
    {
        return isValid ? Resources.Load<Sprite>("LegalStamp") : Resources.Load<Sprite>(Random.Range(0, 2) == 1 ? "BrokenStamp" : "FakeStamp");
    }

    public static string GetRandomCode()
    {
        return Random.Range(0, 99999).ToString("D5");
    }

    public static int GetRandomAge(bool isValid, int baseAge)
    {
        int age = 0;

        if (isValid)
            age = (int)(baseAge * Random.Range(1.0f, 1.7f));
        else age = (int)(baseAge * Random.Range(0.85f, 0.95f));

        return age;
    }

    private static int RandomNumber(int arrayLength)
    {
        return Random.Range(0, arrayLength - 1);
    }
}
