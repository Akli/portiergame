﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static Dictionary<Flaw, IFlaw> Flaws;

    public Dictionary<GameState, IStateManager> StateManagers;
    public static Action<GameState> OnGameStateChange;
    public IStateManager CurrentState;

    public UnitManager UnitManager;

    private void Awake()
    {

        DontDestroyOnLoad(this);
        Flaws = new Dictionary<Flaw, IFlaw>
        {
            {Flaw.Toxicated, new Toxcicated()},
            {Flaw.FakeID, new FakeID()},
            {Flaw.UnderAged, new Underaged()},
            {Flaw.WrongRace, new WrongRace()}
        };


        StateManagers = new Dictionary<GameState, IStateManager>
        {
            {GameState.PauseState, new PauseManager(this)},
            {GameState.MenuState, new MenuManager() },
            {GameState.ActiveState, new MainManager(this) }
        };

        OnGameStateChange += GameStateChangeHandler;

        OnGameStateChange(GameState.ActiveState);

    }

    private void GameStateChangeHandler(GameState newState)
    {
        if (CurrentState != null)
        {
            if(CurrentState != StateManagers[newState])
                CurrentState.OnStateExit();
            else return;
        }

        CurrentState = StateManagers[newState];
        CurrentState.OnStateEnter();
    }

    void Update()
    {
        PauseHandler();
    }

    private void PauseHandler()
    {
        if (Input.GetKeyDown(KeyCode.P) && CurrentState.MyState() == GameState.ActiveState)
            OnGameStateChange(GameState.PauseState);

        else if (Input.GetKeyDown(KeyCode.P) && CurrentState.MyState() == GameState.PauseState)
            OnGameStateChange(GameState.ActiveState);
    }

    void OnGUI()
    {
        GUI.Label(new Rect(250,25, 250,50), "CurrentState: " + CurrentState.MyManagerName());
    }
}

public enum GameState
{
    MenuState,
    ActiveState,
    PauseState,
}
