﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MessageUtility 
{
    public static string GetCaughtMessage(Flaw flaw)
    {
        return !DataController.MessageInformation.ContainsKey(flaw) ? "This race does not exist!" :
            DataController.MessageInformation[flaw].CaughtStrings[RandomNumber(DataController.MessageInformation[flaw].CaughtStrings.Length)];
    }

    public static string GetFalselyDeniedMessage(Race race)
    {
        return !DataController.RaceInformation.ContainsKey(race) ? "This race does not exist!" :
            DataController.RaceInformation[race].DeniedMessages[RandomNumber(DataController.RaceInformation[race].DeniedMessages.Length)];
    }

    public static string GetRandomMessage(Race race)
    {
        return !DataController.RaceInformation.ContainsKey(race) ? "This race does not exist!" :
            DataController.RaceInformation[race].RandomMessages[RandomNumber(DataController.RaceInformation[race].RandomMessages.Length)];
    }


    private static int RandomNumber(int arrayLength)
    {
        return Random.Range(0, arrayLength);
    }
}
