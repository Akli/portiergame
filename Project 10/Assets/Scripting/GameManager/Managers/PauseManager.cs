﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : IStateManager
{
    private GameManager gameManager;

    public PauseManager(GameManager handler)
    {
        gameManager = handler;
    }

    public void OnStateEnter()
    {
        Debug.Log("We've Entered pause state");
        gameManager.UnitManager.isPaused = true;
    }

    public void OnStateExit()
    {
        Debug.Log("We've Exited pause state");
        gameManager.UnitManager.isPaused = false;
    }

    public string MyManagerName()
    {
        return "The pause state";
    }

    public GameState MyState()
    {
        return GameState.PauseState;
    }
}
