﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class MainManager : IStateManager
{
    private bool isInitialized;
    private GameManager gameHandler;

    public MainManager(GameManager gameManager)
    {
        gameHandler = gameManager;
    }

    public void OnStateEnter()
    {
        Debug.Log("You've entered the Main game state!");

        if(!isInitialized)
            InitializeGame();
    }

    public void OnStateExit()
    {
        Debug.Log("You've exited the Main game state!");
    }

    public string MyManagerName()
    {
        return "The main game state";
    }

    public GameState MyState()
    {
        return GameState.ActiveState;
    }

    private void InitializeGame()
    {
        Debug.Log("Game has been initialized!");
        isInitialized = true;
        gameHandler.UnitManager.StartGameloop();
    }
}