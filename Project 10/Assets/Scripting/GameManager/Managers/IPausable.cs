﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


interface IPausable
{
    bool IsActive { get; set; }
    bool isPaused { get; set; }
}
