﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : IStateManager
{
    public void OnStateEnter()
    {
        Debug.Log("We've Entered menu state");
    }

    public void OnStateExit()
    {
        Debug.Log("We've exited menu state");
    }

    public string MyManagerName()
    {
        return "The Menu state";
    }

    public GameState MyState()
    {
        return GameState.MenuState;
    }
}
