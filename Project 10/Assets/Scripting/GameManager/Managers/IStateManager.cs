﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStateManager
{
    void OnStateEnter();
    void OnStateExit();
    string MyManagerName();
    GameState MyState();
}
