﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UnitManager : MonoBehaviour, IPausable
{
    LiniairSpline m_Spline;
    public LiniairSpline Spline { get { return m_Spline; } }

    private UnitBehaviour[] m_UnitsQue;
    public UnitBehaviour[] UnitsQue { get { return m_UnitsQue; } }

    private Vector3[] m_QuePosition;
    public Vector3[] QuePosition { get { return m_QuePosition; } }

    private List<UnitBehaviour> m_UnitsBuffer;
    public List<UnitBehaviour> UnitsBuffer { get { return m_UnitsBuffer; } set { m_UnitsBuffer = value; } }

    public bool IsActive { get; set; }
    public bool isPaused { get; set; }

    public static Action<BaseUnit> OnUnitClick;

    private void Awake()
    {
        init();
        OnUnitClick += ShowUnitId;
    }

    void init()
    {
        isPaused = false;
        m_Spline = GetComponent<LiniairSpline>();
        m_UnitsBuffer = new List<UnitBehaviour>();
        m_UnitsQue = new UnitBehaviour[m_Spline.QueAmount];
    }

    public void StartGameloop()
    {
        IsActive = true;
        StartCoroutine(GameLoop());
    }

    IEnumerator GameLoop()
    {
        while (IsActive)
        {
            while (!isPaused)
            {
                yield return new WaitForSeconds(1.5f);

                if (AddToUnitsQue())
                {

                }


                //Event? No more units
            }

            yield return new WaitForSeconds(.5f);
        }
    }

    public void AddToBuffer(UnitBehaviour Unit)
    {
        Unit.UnitManager = this;
        Unit.gameObject.SetActive(false);
        Unit.transform.parent = transform;

        m_UnitsBuffer.Add(Unit);
    }


    bool AddToUnitsQue()
    {
        if (m_UnitsQue[m_UnitsQue.Length - 1] == null && m_UnitsBuffer.Count > 0)
        {
            m_UnitsBuffer[m_UnitsBuffer.Count - 1].gameObject.SetActive(true);
            m_UnitsBuffer[m_UnitsBuffer.Count - 1].gameObject.transform.position = Spline.QuePosition[m_UnitsQue.Length - 1];
            m_UnitsBuffer[m_UnitsBuffer.Count - 1].CurrentNode = m_UnitsQue.Length - 1;
            m_UnitsBuffer[m_UnitsBuffer.Count - 1].BaseUnit.StartChat();
            m_UnitsQue[m_UnitsQue.Length - 1] = m_UnitsBuffer[m_UnitsBuffer.Count - 1];
            m_UnitsBuffer.RemoveAt(m_UnitsBuffer.Count - 1);

            return true;
        }

        return false;
    }

    public void MoveUnitPositionInQue(UnitBehaviour unit, int nextIndex)
    {
        m_UnitsQue[nextIndex] = unit;
        m_UnitsQue[unit.CurrentNode] = null;
        unit.CurrentNode = nextIndex;
    }

    public void RemoveFrontInQue(bool Allowdin)
    {
        if (m_UnitsQue[0] != null)
        {
            if (Allowdin)
                m_UnitsQue[0].SwitchState(new Accept(m_UnitsQue[0]));
            else
                m_UnitsQue[0].SwitchState(new Denied(m_UnitsQue[0]));
            m_UnitsQue[0] = null;
        }
        else
        {
            Debug.Log("You've tried to remove something that wasn't there ?");
        }
    }

    public BaseUnit GetFrontBaseUnit()
    {
        if (m_UnitsQue[0] == null) return null;

        if (m_UnitsQue.Length > 0)
            return m_UnitsQue[0].BaseUnit;
        else
        {
            Debug.Log("Queue was 0");
            return null;
        }
    }

    void ShowUnitId(BaseUnit unit)
    {
        Identinity.Instance.DisplayIdentinity(unit.information);
    }
}
