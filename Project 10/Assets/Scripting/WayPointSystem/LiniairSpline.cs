﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LiniairSpline : MonoBehaviour
{
    [HideInInspector] public Vector3[] m_Points;
    public Vector3[] Points { get { return m_Points; } }

    [HideInInspector] public Vector3[] m_Directions;
    public Vector3[] Directions { get { return m_Directions; } }

    [HideInInspector] public float[] m_Distances;
    public float[] Distances { get { return m_Distances; } }

    [HideInInspector] public float m_TotalDistance;
    public float TotalDistance { get { return m_TotalDistance; } }

    [HideInInspector] public int m_QueAmount;
    public int QueAmount
    {
        get { return m_QueAmount; }

        set
        {
            m_QueAmount = value >= 0 ? value : m_QueAmount;
            Array.Resize(ref m_QuePosition, m_QueAmount);
            CalculateQuePositions();
        }
    }

    private Vector3[] m_QuePosition;
    public Vector3[] QuePosition { get { return m_QuePosition; } }

    private Vector3[] m_QueDirections;
    public Vector3[] QueDirections { get { return m_QueDirections; } }

    [Range(0.0f, 1.0f)]
    [SerializeField] float m_t;


    private void Awake()
    {
        CalculateQuePositions();
    }

    public void AddPoint()
    {
        if (m_Points == null)
        {
            Array.Resize(ref m_Points, 1);
            m_Points[m_Points.Length - 1] = Vector3.zero;
        }
        else if (m_Points.Length > 0)
        {
            Vector3 point = m_Points[m_Points.Length - 1] + Vector3.right;
            Array.Resize(ref m_Points, m_Points.Length + 1);
            m_Points[m_Points.Length - 1] = point;
        }
        else
        {
            Vector3 point = Vector3.zero;
            Array.Resize(ref m_Points, m_Points.Length + 1);
            m_Points[m_Points.Length - 1] = point;
        }
        CalculateDistances();
    }

    public void RemovePoint()
    {
        if (m_Points == null)
            return;
        if (m_Points.Length > 0)
            Array.Resize(ref m_Points, m_Points.Length - 1);
        CalculateDistances();
    }

    public void CalculateDistances()
    {
        if (m_Points.Length > 0)
        {
            Array.Resize(ref m_Distances, m_Points.Length - 1);
            Array.Resize(ref m_Directions, m_Distances.Length);

            for (int i = 1; i < m_Points.Length; i++)
            {
                m_Directions[i - 1] = (m_Points[i] - m_Points[i - 1]);
                m_Distances[i - 1] = m_Directions[i - 1].magnitude;
            }
        }
        else
        {
            Array.Resize(ref m_Distances, 0);
        }
        CalculateTotalDistances();
    }

    public void CalculateTotalDistances()
    {
        float totalDistance = 0;

        for (int i = 0; i < m_Distances.Length; i++)
        {
            totalDistance += m_Distances[i];
        }

        m_TotalDistance = totalDistance;

        CalculateQuePositions();
    }

    public Vector3 GetInterpolationPoint(float t)
    {
        Mathf.Clamp01(t);
        float distanceOverSpline = m_TotalDistance * t;
        float currentDistance = 0;
        Vector3 InterPolatedPoint = Vector3.zero;

        for (int i = 0; i < m_Distances.Length; i++)
        {
            currentDistance += m_Distances[i];

            if (distanceOverSpline > currentDistance)
            {
                continue;
            }

            if (distanceOverSpline <= currentDistance)
            {
                if (distanceOverSpline == 0)
                {
                    InterPolatedPoint = m_Points[i];                  
                }
                else
                {
                    float remainingDistance = currentDistance - distanceOverSpline;
                    float factor = 1 - (remainingDistance / Distances[i]);
                    InterPolatedPoint = m_Points[i] + m_Directions[i] * factor;
                }
                return transform.TransformPoint(InterPolatedPoint);
            }
        }
        return transform.TransformPoint(InterPolatedPoint);

    }

    public void CalculateQuePositions()
    {
        Array.Resize(ref m_QuePosition, m_QueAmount);
        Array.Resize(ref m_QueDirections, m_QueAmount);
        for (int i = 0; i < m_QuePosition.Length; i++)
        {
            m_QuePosition[i] = GetInterpolationPoint(((float)1 / (float)m_QueAmount) * (float)i);
        }

        for (int i = 0; i < m_QueDirections.Length; i++)
        {

            if (i < m_QueDirections.Length - 1)
                m_QueDirections[i] = m_QuePosition[i + 1] - m_QuePosition[i];
            else
                m_QueDirections[i] = m_QueDirections[i-1];

        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        if (m_QuePosition != null)
        {
            for (int i = 0; i < m_QuePosition.Length; i++)
            {
                Gizmos.DrawSphere(m_QuePosition[i], 1);
                Gizmos.DrawLine(m_QuePosition[i], m_QuePosition[i] + m_QueDirections[i]);
            }
        }
    }
}
