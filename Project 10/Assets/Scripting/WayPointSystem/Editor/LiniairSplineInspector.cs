﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LiniairSpline))]
public class LiniairSplineInspector : Editor
{
    private LiniairSpline WayPointSystem;
    private Transform handleTransform;
    private Quaternion handleRotation;

    private void OnSceneGUI()
    {
        WayPointSystem = target as LiniairSpline;
        handleTransform = WayPointSystem.transform;
        handleRotation = Tools.pivotRotation == PivotRotation.Local ?
        handleTransform.rotation : Quaternion.identity;

        DrawPoints();
    }

    void DrawPoints()
    {
        Handles.color = Color.white;
        if (WayPointSystem.Points != null)
        {
            for (int i = 0; i < WayPointSystem.Points.Length; i++)
            {
                ShowPoint(i);

                if (i > 0)
                    Handles.DrawLine(ShowPoint(i - 1), ShowPoint(i));
            }
        }
    }

    private Vector3 ShowPoint(int index)
    {
        Vector3 point = handleTransform.TransformPoint(WayPointSystem.Points[index]);
        EditorGUI.BeginChangeCheck();
        point = Handles.DoPositionHandle(point, handleRotation);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(WayPointSystem, "Move Point");
            EditorUtility.SetDirty(WayPointSystem);
            WayPointSystem.Points[index] = handleTransform.InverseTransformPoint(point);
            WayPointSystem.CalculateDistances();
        }

        return point;

    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        WayPointSystem = target as LiniairSpline;

        ShowQuePositionsField();

        ShowPointField();
        AddPointButtoon();
        RemovePointButtoon();

        ShowDistanceField();
        ShowDirectionField();
        ShowTotalDistanceField();
    }

    void ShowPointField()
    {
        if (WayPointSystem.Points != null)
        {
            for (int i = 0; i < WayPointSystem.Points.Length; i++)
            {
                EditorGUI.BeginChangeCheck();
                Vector3 point = EditorGUILayout.Vector3Field("Point " + i, WayPointSystem.Points[i]);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(WayPointSystem, "Moved Point");
                    EditorUtility.SetDirty(WayPointSystem);
                    WayPointSystem.Points[i] = point;
                    WayPointSystem.CalculateDistances();
                }
            }
        }
    }

    void ShowDirectionField()
    {
        if (WayPointSystem.Distances != null)
        {
            for (int i = 0; i < WayPointSystem.Distances.Length; i++)
            {
                Vector3 distance = EditorGUILayout.Vector3Field("Direction" + i + "-" + (i + 1), WayPointSystem.Directions[i]);
            }
        }
    }

    void ShowDistanceField()
    {
        if (WayPointSystem.Distances != null)
        {
            for (int i = 0; i < WayPointSystem.Distances.Length; i++)
            {
                float distance = EditorGUILayout.FloatField("Distance" + i + "-" + (i + 1), WayPointSystem.Distances[i]);
            }
        }
    }

    void ShowQuePositionsField()
    {
        EditorGUI.BeginChangeCheck();
        int quePosition = EditorGUILayout.IntField("Que Amount", WayPointSystem.QueAmount);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(WayPointSystem, "Moved Point");
            EditorUtility.SetDirty(WayPointSystem);

            WayPointSystem.QueAmount = quePosition;
        }
    }

    void ShowTotalDistanceField()
    {
        float Distance = EditorGUILayout.FloatField("Total Distance", WayPointSystem.TotalDistance);
    }

    void AddPointButtoon()
    {
        if (GUILayout.Button("Add Point"))
        {
            Undo.RecordObject(WayPointSystem, "Add Point");
            EditorUtility.SetDirty(WayPointSystem);
            WayPointSystem.AddPoint();
        }
    }

    void RemovePointButtoon()
    {
        if (GUILayout.Button("Remove Point"))
        {
            Undo.RecordObject(WayPointSystem, "Removed Point");
            EditorUtility.SetDirty(WayPointSystem);
            WayPointSystem.RemovePoint();
        }
    }
}
